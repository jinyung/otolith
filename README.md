Otolith
=======

R package for identification of fish otolith using digital images

Please refer to the [Wiki pages](https://bitbucket.org/jinyung/otolith/wiki/Home) for details of the project